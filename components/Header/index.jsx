import { SiteLogo } from "../SiteLogo";

export function Header() {
    return(
        <header>
            <SiteLogo />
        </header>
    )
}