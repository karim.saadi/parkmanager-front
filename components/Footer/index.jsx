import { SiteLogo } from "../SiteLogo";

export function Footer() {
    return (
        <footer>
            <SiteLogo />
        </footer>
    )
}