import Head from 'next/head'
import Container from '@mui/material/Container';
import { Header } from '../../../components/Header';
import { Footer } from '../../../components/Footer';

export default function Signup() {
  return (
    <div className="container">
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Header />
      <Container fixed>
        <main>

          <h1 className="title">
            Signup
          </h1>
        </main>
      </Container>
      <Footer />

      <style jsx>
        {`
        @import 'https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css';
        `}
      </style>

    </div>
  )
}
